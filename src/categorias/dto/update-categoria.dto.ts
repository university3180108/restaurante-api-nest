// import { PartialType } from '@nestjs/mapped-types';
// import { CreateCategoriaDto } from './create-categoria.dto';

import { IsString, MinLength } from 'class-validator';

// export class UpdateCategoriaDto extends PartialType(CreateCategoriaDto) {}
export class UpdateCategoriaDto {
  @IsString()
  @MinLength(1)
  readonly categoria: string;
}
