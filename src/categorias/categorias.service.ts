import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCategoriaDto } from './dto/create-categoria.dto';
import { UpdateCategoriaDto } from './dto/update-categoria.dto';
import { Categoria } from './entities/categoria.entity';

@Injectable()
export class CategoriasService {
  private categorias: Categoria[] = [
    // {
    //   id: crypto.randomUUID(),
    //   categoria: 'categoria_1',
    //   createdAt: new Date().getTime(),
    // },
  ];

  create(createCategoriaDto: CreateCategoriaDto) {
    const categoria: Categoria = {
      id: crypto.randomUUID(),
      categoria: createCategoriaDto.categoria.toLowerCase(),
      createdAt: new Date().getTime(),
    };

    this.categorias.push(categoria);

    return categoria;
  }

  findAll() {
    return this.categorias;
  }

  findOne(id: string) {
    const categoria = this.categorias.find((cat) => cat.id === id);
    if (!categoria)
      throw new NotFoundException(`Categoria con id ${id} no encontrada`);

    return categoria;
  }

  update(id: string, updateCategoriaDto: UpdateCategoriaDto) {
    let categoriaDB = this.findOne(id);
    this.categorias = this.categorias.map((cat) => {
      if (cat.id === id) {
        categoriaDB.updatedAt = new Date().getTime();
        categoriaDB = {
          ...categoriaDB,
          ...updateCategoriaDto,
          id,
        };
      }
      return cat;
    });
    return categoriaDB;
  }

  remove(id: string) {
    this.findOne(id);
    this.categorias.filter((cat) => cat.id !== id);
    return `Se elimino la categoria con id: ${id}`;
  }
}
