export class Categoria {
  id: string;
  categoria: string;

  createdAt: number;
  updatedAt?: number;
}
