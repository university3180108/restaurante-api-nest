import { Module } from '@nestjs/common';
import { ProductosModule } from './productos/productos.module';
import { CategoriasModule } from './categorias/categorias.module';
import { SeedModule } from './seed/seed.module';
import { DrizzleModule } from './drizzle/drizzle.module';
import { UsuariosModule } from './usuarios/usuarios.module';

@Module({
  imports: [
    ProductosModule,
    CategoriasModule,
    SeedModule,
    DrizzleModule,
    UsuariosModule,
  ],
  controllers: [],
  providers: [],
  exports: [],
})
export class AppModule {}
