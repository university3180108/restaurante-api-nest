import {
  pgTable,
  uuid,
  serial,
  text,
  integer,
  real,
  timestamp,
  date,
} from 'drizzle-orm/pg-core';

export const proveedor = pgTable('proveedor', {
  id: serial('id').primaryKey(),
  nombre: text('nombre').notNull(),
  celular: text('celular'),
  telefono: text('telefono'),
  correo: text('correo'),
  direccion: text('direccion'),
});

export const productoProveedor = pgTable('productoProveedor', {
  productoId: integer('producto_id').references(() => producto.id),
  proveedorId: integer('proveedor_id').references(() => proveedor.id),
});

export const producto = pgTable('producto', {
  id: serial('id').primaryKey(),
  nombre: text('nombre').notNull(),
  descripcion: text('descripcion').notNull(),
  cantidad: integer('cantidad'),
  categoria: text('categoria').notNull(),
  precio: real('precio').notNull(),
  urlImagen: text('urlImagen'),
});

export const menu = pgTable('menu', {
  id: serial('id').primaryKey(),
  descripcion: text('descripcion').notNull(),
  fecha: date('fecha').notNull(),
  estado: text('estado').notNull(),
});

export const menuProducto = pgTable('menuProducto', {
  id: serial('id').primaryKey(),
  cantidadPreparada: integer('cantidadPreparada').notNull(),
  cantidadVendida: integer('cantidadVendida').notNull(),
  menuId: integer('menu_id').references(() => menu.id),
  productoId: integer('producto_id').references(() => producto.id),
});

export const usuario = pgTable('usuario', {
  id: uuid('id').primaryKey(),
  nombre: text('nombre').notNull(),
  primerApellido: text('primerApellido').notNull(),
  segundoApellido: text('segundoApellido'),
  correo: text('correo').notNull(),
  password: text('password').notNull(),
  rol: text('rol').notNull(),
});

export const reservaPlatos = pgTable('reservaPlatos', {
  id: serial('id').primaryKey(),
  cantidad: integer('cantidad').notNull(),
  reservaId: integer('reserva_id').references(() => reserva.id),
  productoId: integer('producto_id').references(() => producto.id),
});

export const reserva = pgTable('reserva', {
  id: serial('id').primaryKey(),
  estado: text('estado').notNull(),
  cantidadPersonas: integer('cantidadPersonas').notNull(),
  fechaHora: timestamp('fechaHora').notNull(),
  mesa: integer('mesa').notNull(),
});

export const cliente = pgTable('cliente', {
  ci: text('ci').primaryKey(),
  nombre: text('nombre').notNull(),
  celular: text('celular'),
  puntos: integer('puntos'),
  direccion: text('direccion'),
  notasEntrega: text('notasEntrega'),
  razonSocial: text('razonSocial'),
  nit: text('nit'),
});

export const productoCombo = pgTable('productoCombo', {
  productoId: integer('producto_id').references(() => producto.id),
  comboId: integer('combo_id').references(() => combo.id),
});

export const combo = pgTable('combo', {
  id: serial('id').primaryKey(),
  nombre: text('nombre').notNull(),
  precio: real('precio').notNull(),
  estado: text('estado').notNull(),
  urlImagen: text('urlImagen'),
});

export const detalleCombo = pgTable('detalleCombo', {
  estado: text('estado').notNull(),
  calificacion: integer('calificacion'),
  cantidad: integer('cantidad').notNull(),
  detalleId: integer('detalle_id').references(() => detallePedido.id),
  comboId: integer('combo_id').references(() => combo.id),
});

export const detallePlatos = pgTable('detallePlatos', {
  estado: text('estado').notNull(),
  calificacion: integer('calificacion'),
  cantidad: integer('cantidad').notNull(),
  detalleId: integer('detalle_id').references(() => detallePedido.id),
  menuProductoId: integer('menuProductoId').references(() => menuProducto.id),
});

export const detallePedido = pgTable('detallePedido', {
  id: serial('id').primaryKey(),
  total: real('total').notNull(),
  productoId: integer('producto_id').references(() => producto.id),
  pedidoId: integer('pedido_id').references(() => pedido.id),
});

export const pedido = pgTable('pedido', {
  id: serial('id').primaryKey(),
  fechaHoraPedido: timestamp('fechaHoraPedido').notNull(),
  fechaHoraEntrega: timestamp('fechaHoraEntrega').notNull(),
  tipo: text('tipo').notNull(),
  estado: text('estado').notNull(),
  mesaId: integer('mesa_id').references(() => mesa.id),
  pagoId: integer('pago_id').references(() => pago.id),
  clienteCi: text('cliente_ci').references(() => cliente.ci),
});

export const mesa = pgTable('mesa', {
  id: serial('id').primaryKey(),
  numero: integer('numero').notNull(),
  estado: text('estado').notNull(),
});

export const pago = pgTable('pago', {
  id: serial('id').primaryKey(),
  tipo: text('tipo').notNull(),
  monto: real('monto').notNull(),
});

export const factura = pgTable('factura', {
  id: serial('id').primaryKey(),
  fechaHora: timestamp('fechaHora').notNull(),
  razonSocial: text('razonSocial').notNull(),
  nit: text('nit').notNull(),
  pagoId: integer('pago_id').references(() => pago.id),
});
