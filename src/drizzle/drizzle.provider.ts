import 'dotenv/config';
import { drizzle } from 'drizzle-orm/postgres-js';
import * as postgres from 'postgres';
import * as schema from 'src/drizzle/schema';

export const DrizzleAsyncProvider = 'drizzleProvider';
export const drizzleProvider = [
  {
    provide: DrizzleAsyncProvider,
    useFactory: async () => {
      const connectionString = process.env.DATABASE_URL as string;
      const client = postgres(connectionString, { prepare: false });
      const db = drizzle(client, { schema });
      return db;
    },
  },
];
