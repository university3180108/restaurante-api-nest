import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { Producto } from './interfaces/plato.interface';
import { CreateProductoDto, UpdateProductoDto } from './dto';

import { DrizzleAsyncProvider } from 'src/drizzle/drizzle.provider';
import { PostgresJsDatabase } from 'drizzle-orm/postgres-js';
import * as schema from 'src/drizzle/schema';
import { eq } from 'drizzle-orm';

@Injectable()
export class ProductosService {
  constructor(
    @Inject(DrizzleAsyncProvider) private db: PostgresJsDatabase<typeof schema>,
  ) {}

  public async getAllProducts(): Promise<any> {
    const plato = await this.db.query.producto.findMany();
    return plato;
  }

  public async getProductById(id: number): Promise<Producto> {
    const producto = await this.db.query.producto.findFirst({
      where: eq(schema.producto.id, id),
    });

    if (!producto)
      throw new NotFoundException(`Plato con id: ${id}, no encontrado`);

    return producto;
  }

  public async create(createDishDto: CreateProductoDto) {
    await this.db.insert(schema.producto).values(createDishDto);
    return createDishDto;
  }

  public async update(id: number, updateDishDto: UpdateProductoDto) {
    const dishDB = this.getProductById(id);
    if (!dishDB)
      throw new NotFoundException(
        `Plato con id: ${id}, no se pudo actualizar porque no existe`,
      );
    await this.db
      .update(schema.producto)
      .set(updateDishDto)
      .where(eq(schema.producto.id, id));
    return { id, ...updateDishDto };
  }

  public async delete(id: number) {
    const dishDB = this.getProductById(id);
    if (!dishDB)
      throw new NotFoundException(
        `Plato con id: ${id}, no se pudo eliminar porque no existe`,
      );
    await this.db.delete(schema.producto).where(eq(schema.producto.id, id));
    return { message: `Plato con id: ${id}, ha sido eliminado` };
  }
}
