import { Module } from '@nestjs/common';
import { ProductosController } from './productos.controller';
import { ProductosService } from './productos.service';

import { drizzleProvider } from 'src/drizzle/drizzle.provider';

@Module({
  controllers: [ProductosController],
  providers: [...drizzleProvider, ProductosService],
  exports: [ProductosService],
})
export class ProductosModule {}
