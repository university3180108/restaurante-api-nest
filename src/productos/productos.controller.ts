import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
} from '@nestjs/common';
import { ProductosService } from './productos.service';
import { CreateProductoDto } from './dto/create-producto.dto';
import { UpdateProductoDto } from './dto/update-producto.dto';

@Controller('productos')
export class ProductosController {
  constructor(private readonly platosService: ProductosService) {}
  @Get()
  getAllDishes() {
    return this.platosService.getAllProducts();
  }

  @Get(':id')
  getDishById(@Param('id', ParseIntPipe) id: number) {
    return this.platosService.getProductById(id);
  }

  @Post()
  createDish(@Body() createDishDto: CreateProductoDto) {
    return this.platosService.create(createDishDto);
  }

  @Patch(':id')
  updateDish(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateDishDto: UpdateProductoDto,
  ) {
    return this.platosService.update(id, updateDishDto);
  }

  @Delete(':id')
  deleteDish(@Param('id', ParseIntPipe) id: number) {
    return this.platosService.delete(id);
  }
}
