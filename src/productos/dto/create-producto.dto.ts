import { IsNumber, IsString } from 'class-validator';
export class CreateProductoDto {
  @IsString()
  readonly nombre: string;
  @IsString()
  readonly descripcion: string;
  @IsString()
  readonly urlImagen: string;
  @IsNumber()
  readonly precio: number;
  @IsString()
  readonly categoria: string;
  @IsNumber()
  readonly cantidad: number;
}
