export class Producto {
  id: number;
  nombre: string;
  descripcion: string;
  cantidad: number;
  urlImagen: string;
  precio: number;
  categoria: string;
}
