import { Module } from '@nestjs/common';
import { SeedService } from './seed.service';
import { SeedController } from './seed.controller';
import { CategoriasModule } from 'src/categorias/categorias.module';
import { ProductosModule } from 'src/productos/productos.module';

@Module({
  controllers: [SeedController],
  providers: [SeedService],
  imports: [CategoriasModule, ProductosModule],
})
export class SeedModule {}
