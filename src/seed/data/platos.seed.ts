import { Producto } from '../../productos/interfaces/plato.interface';

export const PRODUCTOS_SEED: Producto[] = [
  {
    id: 1,
    nombre: 'Picante de pollo',
    descripcion:
      'El picante de pollo es una de las muchas recetas criollas originarias del oeste de Bolivia que se caracteriza por su aroma y sabor picante.',
    urlImagen:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ_tLhvRr_O_gh6KCIW_xUO0BgPq8_HN-23ooMSYjVbBw&s',
    precio: 30,
    categoria: 'Plato tradicional',
    cantidad: 30,
  },
  {
    id: 2,
    nombre: 'Pique macho',
    descripcion:
      'El pique macho o pique a lo macho es un plato típico de Bolivia.1​ Consiste en trozos de carne de vaca y patatas fritas. También se le añade cebolla, locoto, huevos duros, queso cortado, mostaza, mayonesa y kétchup.',
    urlImagen:
      'https://chipabythedozen.com/wp-content/uploads/2020/07/Pique-Macho-Bolivia.jpg',
    precio: 100,
    categoria: 'Plato tradicional',
    cantidad: 20,
  },
  {
    id: 3,
    nombre: 'Coca Cola 2 litros',
    descripcion: 'Coca cola tradicional de 2 litros.',
    urlImagen:
      'https://farmacorp.com/cdn/shop/files/909699_1200x1200.jpg?v=1714434898',
    precio: 20,
    categoria: 'Bebidas',
    cantidad: 30,
  },
  {
    id: 4,
    nombre: 'Fanta 2 litros',
    descripcion: 'Fanta tradicional de 2 litros.',
    urlImagen: 'https://fsa.bo/productos/03282-01.jpg',
    precio: 20,
    categoria: 'Bebidas',
    cantidad: 30,
  },
  {
    id: 5,
    nombre: 'Cerveza paceña en lata',
    descripcion: 'Cerveza paceña en lata.',
    urlImagen:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRki6pRR1DdaS0hpkXkqtzWpZqzV_rG2Xe6JIxxto7ALQ&s',
    precio: 10,
    categoria: 'Bebidas',
    cantidad: 30,
  },
  {
    id: 6,
    nombre: 'Saice',
    descripcion:
      'Es el plato más representativo de la culinaria tarijeña. Elaborado a base de carne picada, papa, arveja, cebolla, condimentos y ají colorado.',
    urlImagen: 'https://www.eabolivia.com/images/stories/a33/saice-tarija.jpg',
    precio: 20,
    categoria: 'Plato Tradicional',
    cantidad: 30,
  },
  {
    id: 7,
    nombre: 'Charkecan',
    descripcion:
      'Es un plato típico de Oruro cuya característica principal es la pre-cocción de la carne anteriormente al resto de la preparación.',
    urlImagen:
      'https://www.cocina-boliviana.com/base/stock/Recipe/479-image/479-image_web.jpg',
    precio: 30,
    categoria: 'Plato Tradicional',
    cantidad: 40,
  },
  {
    id: 8,
    nombre: 'Mondongo',
    descripcion:
      'El Mondongo chuquisaqueño es un plato fuerte muy popular en el Departamento de Chuquisaca, Bolivia, que se elabora a partir de carne de cerdo como ingrediente principal.',
    urlImagen:
      'https://azafranbolivia.com/wp-content/uploads/2022/11/todos-santos-bolivia-gastronomia-comida-tradicion.jpg',
    precio: 30,
    categoria: 'Plato Tradicional',
    cantidad: 40,
  },
  {
    id: 9,
    nombre: 'Chorizoo Chuquisaqueño',
    descripcion:
      'Los Chorizos Chuquisaqueños son probablemente la insignia principal de la comida de Chuquisaca. Tienen un sabor picante que deleita paladares en todo Bolivia.',
    urlImagen:
      'https://www.cocina-boliviana.com/base/stock/Recipe/444-image/444-image_web.jpg.webp',
    precio: 25,
    categoria: 'Plato Tradicional',
    cantidad: 20,
  },
  {
    id: 10,
    nombre: 'Chicharon de Cerdo',
    descripcion:
      'El Chicharron de Cerdo es un plato casi exclusivo de la region de los valles bolivianos, sobre todo por los ingredientes que utiliza.',
    urlImagen:
      'https://d10nbigpolte6j.cloudfront.net/media/catalog/product/c/h/chicharron_cerdo_yo_chef.jpg',
    precio: 30,
    categoria: 'Plato Tradicional',
    cantidad: 20,
  },
  {
    id: 11,
    nombre: 'Hamburguesa doble',
    descripcion:
      'Hamburguesa doble de carne de res, pan, queso chedar, lechuga y mostaza.',
    urlImagen:
      'https://d31npzejelj8v1.cloudfront.net/media/recipemanager/recipe/1687289598_doble-carne.jpg',
    precio: 35,
    categoria: 'Comida Rapida',
    cantidad: 15,
  },
  {
    id: 12,
    nombre: 'Pizza de queso',
    descripcion:
      'Pizza de queso mozarella, salsa de tomate, con el mejor sabor.',
    urlImagen:
      'https://www.foodandwine.com/thmb/Wd4lBRZz3X_8qBr69UOu2m7I2iw=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/classic-cheese-pizza-FT-RECIPE0422-31a2c938fc2546c9a07b7011658cfd05.jpg',
    precio: 40,
    categoria: 'Comida Rapida',
    cantidad: 25,
  },
  {
    id: 13,
    nombre: 'Milanesa Napolitana',
    descripcion:
      'Milanesa con una cobertura de queso fundido, salsa de tomate y perfume del orégano esta milanesa no tienen quien le gane, acomapañada de una racion de papas fritas.',
    urlImagen:
      'https://resizer.glanacion.com/resizer/v2/milanesa-a-la-napolitana-con-guarnicion-de-papas-VLWFAANIWBGPFO4CSUHS7RYVVQ.jpg?auth=335fda04cf2733e39d11ca0ba979c1d0a8a55e6cdec15e4d5b00cfd59fbf9ed8&width=768&height=512&quality=70&smart=true',
    precio: 35,
    categoria: 'Comida Rapida',
    cantidad: 25,
  },
  {
    id: 14,
    nombre: 'huari miel',
    descripcion: 'Cerveza sabor miel en botella',
    urlImagen:
      'https://www.fidalga.com/cdn/shop/products/18f61bd3-3cbb-4604-888e-a69cc53c2464_1af3b4db-30f2-4f18-a5d6-664a03e4d1c0.png?v=1653345086',
    precio: 0,
    categoria: 'Bebidas',
    cantidad: 2,
  },
  {
    id: 15,
    nombre: 'Sprite de 2L',
    descripcion: 'Gaseosa sabor limon de dos litros',
    urlImagen:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRNn-qAWVw0UIUzBrBLV2sgjrAW9TyrIdw3SQ&s',
    precio: 15,
    categoria: 'Bebidas',
    cantidad: 10,
  },
  {
    id: 17,
    nombre: 'Salvieti',
    descripcion: 'bebida',
    urlImagen:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSFSfOQIqkGsaE_ifcn34gKaG-Zy45Eig2tYA&s',
    precio: 8,
    categoria: 'Bebidas',
    cantidad: 2,
  },
  {
    id: 16,
    nombre: 'Salvieti',
    descripcion: 'bebida',
    urlImagen:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSFSfOQIqkGsaE_ifcn34gKaG-Zy45Eig2tYA&s',
    precio: 8,
    categoria: 'Bebidas',
    cantidad: 2,
  },
  {
    id: 18,
    nombre: 'Salvieti',
    descripcion: 'bebida',
    urlImagen:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSFSfOQIqkGsaE_ifcn34gKaG-Zy45Eig2tYA&s',
    precio: 8,
    categoria: 'Bebidas',
    cantidad: 2,
  },
];
