import { Injectable } from '@nestjs/common';
import { ProductosService } from '../productos/productos.service';
import { CategoriasService } from '../categorias/categorias.service';

import { PRODUCTOS_SEED } from './data';

@Injectable()
export class SeedService {
  constructor(
    private readonly productosService: ProductosService,
    private readonly categoriasService: CategoriasService,
  ) {}
  populateDB() {
    PRODUCTOS_SEED.forEach((producto) => {
      this.productosService.create(producto);
    });
    return 'seed executed successfully!';
  }
}
