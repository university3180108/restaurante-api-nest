export class Usuario {
  id: string;
  nombre: string;
  primerApellido: string;
  segundoApellido: string;
  correo: string;
  password: string;
  rol: string;
}
