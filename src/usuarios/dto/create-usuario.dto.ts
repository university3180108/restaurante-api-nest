import { IsEmail, IsString } from 'class-validator';

export class CreateUsuarioDto {
  @IsString()
  readonly nombre: string;
  @IsString()
  readonly primerApellido: string;
  @IsString()
  readonly segundoApellido: string;
  @IsEmail()
  readonly correo: string;
  @IsString()
  readonly password: string;
  @IsString()
  readonly rol: string;
}
