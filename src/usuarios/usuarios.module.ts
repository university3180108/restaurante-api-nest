import { Module } from '@nestjs/common';
import { UsuariosService } from './usuarios.service';
import { UsuariosController } from './usuarios.controller';

import { drizzleProvider } from 'src/drizzle/drizzle.provider';

@Module({
  controllers: [UsuariosController],
  providers: [...drizzleProvider, UsuariosService],
})
export class UsuariosModule {}
