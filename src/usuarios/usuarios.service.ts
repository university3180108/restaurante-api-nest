import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { Usuario } from './entities/usuario.entity';

import { DrizzleAsyncProvider } from 'src/drizzle/drizzle.provider';
import { PostgresJsDatabase } from 'drizzle-orm/postgres-js';
import * as schema from 'src/drizzle/schema';
import { eq } from 'drizzle-orm';

@Injectable()
export class UsuariosService {
  constructor(
    @Inject(DrizzleAsyncProvider) private db: PostgresJsDatabase<typeof schema>,
  ) {}
  async create(createUsuarioDto: CreateUsuarioDto): Promise<Usuario> {
    const usuario = await this.db
      .insert(schema.usuario)
      .values({ id: '112341234', ...createUsuarioDto })
      .returning();
    return usuario[0];
  }

  async findAll(): Promise<Omit<Usuario, 'password'>[]> {
    const usuarios = await this.db.query.usuario.findMany({
      columns: {
        password: false,
      },
    });
    return usuarios;
  }

  findOne(id: string) {
    const usuario = this.db.query.usuario.findFirst({
      columns: {
        password: false,
      },
      where: eq(schema.usuario.id, id),
    });
    if (!usuario)
      throw new NotFoundException(`Usuario con id: ${id}, no encontrado`);

    return usuario;
  }

  async update(
    id: string,
    updateUsuarioDto: UpdateUsuarioDto,
  ): Promise<Usuario> {
    this.findOne(id);
    const updatedUser = await this.db
      .update(schema.usuario)
      .set(updateUsuarioDto)
      .where(eq(schema.usuario.id, id))
      .returning();
    return updatedUser[0];
  }

  async remove(id: string) {
    this.findOne(id);
    await this.db.delete(schema.usuario).where(eq(schema.usuario.id, id));
    return {
      message: `Se elimino el usuario con id: ${id}`,
    };
  }
}
