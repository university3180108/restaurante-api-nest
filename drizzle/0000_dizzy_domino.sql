CREATE TABLE IF NOT EXISTS "cliente" (
	"ci" text PRIMARY KEY NOT NULL,
	"nombre" text NOT NULL,
	"celular" text,
	"puntos" integer,
	"direccion" text,
	"notasEntrega" text,
	"razonSocial" text,
	"nit" text
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "combo" (
	"id" serial PRIMARY KEY NOT NULL,
	"nombre" text NOT NULL,
	"precio" real NOT NULL,
	"estado" text NOT NULL,
	"urlImagen" text
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "detalleCombo" (
	"estado" text NOT NULL,
	"calificacion" integer,
	"cantidad" integer NOT NULL,
	"detalle_id" integer,
	"combo_id" integer
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "detallePedido" (
	"id" serial PRIMARY KEY NOT NULL,
	"total" real NOT NULL,
	"producto_id" integer,
	"pedido_id" integer
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "detallePlatos" (
	"estado" text NOT NULL,
	"calificacion" integer,
	"cantidad" integer NOT NULL,
	"detalle_id" integer,
	"menuProductoId" integer
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "factura" (
	"id" serial PRIMARY KEY NOT NULL,
	"fechaHora" timestamp NOT NULL,
	"razonSocial" text NOT NULL,
	"nit" text NOT NULL,
	"pago_id" integer
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "menu" (
	"id" serial PRIMARY KEY NOT NULL,
	"descripcion" text NOT NULL,
	"fecha" date NOT NULL,
	"estado" text NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "menuProducto" (
	"id" serial PRIMARY KEY NOT NULL,
	"cantidadPreparada" integer NOT NULL,
	"cantidadVendida" integer NOT NULL,
	"menu_id" integer,
	"producto_id" integer
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "mesa" (
	"id" serial PRIMARY KEY NOT NULL,
	"numero" integer NOT NULL,
	"estado" text NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "pago" (
	"id" serial PRIMARY KEY NOT NULL,
	"tipo" text NOT NULL,
	"monto" real NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "pedido" (
	"id" serial PRIMARY KEY NOT NULL,
	"fechaHoraPedido" timestamp NOT NULL,
	"fechaHoraEntrega" timestamp NOT NULL,
	"tipo" text NOT NULL,
	"estado" text NOT NULL,
	"mesa_id" integer,
	"pago_id" integer,
	"cliente_ci" text
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "producto" (
	"id" serial PRIMARY KEY NOT NULL,
	"nombre" text NOT NULL,
	"descripcion" text NOT NULL,
	"cantidad" integer,
	"categoria" text NOT NULL,
	"precio" real NOT NULL,
	"urlImagen" text
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "productoCombo" (
	"producto_id" integer,
	"combo_id" integer
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "productoProveedor" (
	"producto_id" integer,
	"proveedor_id" integer
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "proveedor" (
	"id" serial PRIMARY KEY NOT NULL,
	"nombre" text NOT NULL,
	"celular" text,
	"telefono" text,
	"correo" text,
	"direccion" text
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "reserva" (
	"id" serial PRIMARY KEY NOT NULL,
	"estado" text NOT NULL,
	"cantidadPersonas" integer NOT NULL,
	"fechaHora" timestamp NOT NULL,
	"mesa" integer NOT NULL
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "reservaPlatos" (
	"id" serial PRIMARY KEY NOT NULL,
	"cantidad" integer NOT NULL,
	"reserva_id" integer,
	"producto_id" integer
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "usuario" (
	"id" uuid PRIMARY KEY NOT NULL,
	"nombre" text NOT NULL,
	"primerApellido" text NOT NULL,
	"segundoApellido" text,
	"correo" text NOT NULL,
	"password" text NOT NULL,
	"rol" text NOT NULL
);
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "detalleCombo" ADD CONSTRAINT "detalleCombo_detalle_id_detallePedido_id_fk" FOREIGN KEY ("detalle_id") REFERENCES "public"."detallePedido"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "detalleCombo" ADD CONSTRAINT "detalleCombo_combo_id_combo_id_fk" FOREIGN KEY ("combo_id") REFERENCES "public"."combo"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "detallePedido" ADD CONSTRAINT "detallePedido_producto_id_producto_id_fk" FOREIGN KEY ("producto_id") REFERENCES "public"."producto"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "detallePedido" ADD CONSTRAINT "detallePedido_pedido_id_pedido_id_fk" FOREIGN KEY ("pedido_id") REFERENCES "public"."pedido"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "detallePlatos" ADD CONSTRAINT "detallePlatos_detalle_id_detallePedido_id_fk" FOREIGN KEY ("detalle_id") REFERENCES "public"."detallePedido"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "detallePlatos" ADD CONSTRAINT "detallePlatos_menuProductoId_menuProducto_id_fk" FOREIGN KEY ("menuProductoId") REFERENCES "public"."menuProducto"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "factura" ADD CONSTRAINT "factura_pago_id_pago_id_fk" FOREIGN KEY ("pago_id") REFERENCES "public"."pago"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "menuProducto" ADD CONSTRAINT "menuProducto_menu_id_menu_id_fk" FOREIGN KEY ("menu_id") REFERENCES "public"."menu"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "menuProducto" ADD CONSTRAINT "menuProducto_producto_id_producto_id_fk" FOREIGN KEY ("producto_id") REFERENCES "public"."producto"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "pedido" ADD CONSTRAINT "pedido_mesa_id_mesa_id_fk" FOREIGN KEY ("mesa_id") REFERENCES "public"."mesa"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "pedido" ADD CONSTRAINT "pedido_pago_id_pago_id_fk" FOREIGN KEY ("pago_id") REFERENCES "public"."pago"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "pedido" ADD CONSTRAINT "pedido_cliente_ci_cliente_ci_fk" FOREIGN KEY ("cliente_ci") REFERENCES "public"."cliente"("ci") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "productoCombo" ADD CONSTRAINT "productoCombo_producto_id_producto_id_fk" FOREIGN KEY ("producto_id") REFERENCES "public"."producto"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "productoCombo" ADD CONSTRAINT "productoCombo_combo_id_combo_id_fk" FOREIGN KEY ("combo_id") REFERENCES "public"."combo"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "productoProveedor" ADD CONSTRAINT "productoProveedor_producto_id_producto_id_fk" FOREIGN KEY ("producto_id") REFERENCES "public"."producto"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "productoProveedor" ADD CONSTRAINT "productoProveedor_proveedor_id_proveedor_id_fk" FOREIGN KEY ("proveedor_id") REFERENCES "public"."proveedor"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "reservaPlatos" ADD CONSTRAINT "reservaPlatos_reserva_id_reserva_id_fk" FOREIGN KEY ("reserva_id") REFERENCES "public"."reserva"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "reservaPlatos" ADD CONSTRAINT "reservaPlatos_producto_id_producto_id_fk" FOREIGN KEY ("producto_id") REFERENCES "public"."producto"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
